Automatically gives players gear on respawn, based on their infantryman's class

Probable config in mission.cfg:


cfgGear {
    common {
        weapon = ...
        items = "Map, Compass, Pocket Calculator"
    };
    
    opfor_rifleman : common {
        pistol = "whatever opfor's pistol is"
    }
    
    opfor_mger : opfor_rifleman {
        primary = mx2000
        mags = ...
    }
    
    blufor_common : common {
        pistol = "whatever.."
    }
};
