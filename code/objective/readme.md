Ticketing system for mission

Lose 1 ticket on death for every area the enemy owns.


timelimit = 3600; // 1 hour time limit
cfgTickets {
    opfor = 300;
    blufor = -1; // infinite tickets
};